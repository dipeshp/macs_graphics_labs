# Graphics Lab2 Opengl

## Running the program in a mac environment

``` bash
$ clang++ -framework Cocoa -framework OpenGL -framework IOKit -o demo main.cpp -I /usr/local/include /usr/local/Cellar/glew/2.1.0_1/lib/libGLEW.a /usr/local/Cellar/glfw/3.3.2/lib/libglfw.3.3.dylib -framework CoreVideo

$ ./main
```

